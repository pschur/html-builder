# HTML Builder

The Element looks like this:
```json
{
    "tag": "<TAG>",
    "a": {}, // Tag Attributes
    "c": []  // Tag Content (can also be a string)
}
```

## Examples

### A-TAG
A simple Anker-Tag pointing to `https://google.de`
```json
{
    "tag": "a",
    "a": {
        "href": "https://google.de"
    },
    "c": "Google"
}
```
```html
<a href="https://google.de">Google</a>
```

### Input (Self Closing)
A simple Input
```json
{
    "tag": "input",
    "a": {
        "type": "text",
        "placeholder": "This is a Demo"
    },
    "self_closing": true
}
```
```html
<input type="text" placeholder="This is a Demo" />
````
