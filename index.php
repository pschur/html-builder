<?php

// require __DIR__.'/manage.php';

$app = new stdClass;
$app->file = __DIR__.'/demo.json';
$app->with_line_breaks = false;
$app->builder = new stdClass;
$app->builder->html = new class {
    public function make(array $item){
        $out = $this->tag($item['tag'], $item['a'] ?? [], isset($item['self_closing']) ? true : false);
        
        $out .= is_array($item['c']) ? $this->content($item['c']) : $item['c'];
        
        return $out.'</'.$item['tag'].'>';
    }
    
    public function tag(string $tag, array $attrs = [], bool $self_closing = false){
        $out = '<'.$tag;
        
        if (isset($attrs)) {
            foreach($attrs as $key => $value){
                $out .= " $key=\"$value\"";
            }
        }
        
        if ($self_closing){
            $out .= " /";
            
            return $out;
        }
        
        $out .= " >";
        
        return $out;
    }
    
    public function content(array $content){
        foreach($content as $item){
            $out .= $this->make($item);
        }
    }
};


$app->content = json_encode(file_get_contents($app->file), true);

if(isset($app->content['tag'])){
    $html = $app->builder->html->make($app->content);
} else {
    die("This file is not file which can be build!");
}

mkdir(__DIR__.'/build');
file_put_contents(__DIR__.'/build/index.html', $html);